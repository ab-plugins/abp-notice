# ABP Notice

Add 4 MyCodes in MyBB

Each MyCode can have a title and will display your notice in a colored box.

## Examples

```[warn]This is a simple warn box[/warn]
[warn=a title]This is a simple warn box with a title[/warn]
[info]This is a simple info box[/info]
[info=a title]This is a simple info box with a title[/info]
[error]This is a simple error box[/error]
[error=a title]This is a simple error box[/error]
[success]This is a simple success box[/success]
[success=a title]This is a simple success box[/success]```

