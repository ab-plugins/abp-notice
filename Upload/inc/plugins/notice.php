<?php
/************************************************************
* author       : CrazyCat
* plugin       : ABP Notice MyCode
* version      : 1.0
*************************************************************/
if(!defined("IN_MYBB"))
{
	die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');
}

define('CN_ABPNOTICE', str_replace('.php', '', basename(__FILE__)));
$plugins->add_hook("parse_message", "notice_run");

function notice_info ()
{
	return array (
		'name'		=> 'ABP Notice MyCode',
		'description'	=> 'Adds several for notice in post: Warn, Success, Error, Info',
		'website'	=> 'http://ab-plugin.cc/ABP-Notice-t-24.html',
		'author'	=> 'CrazyCat',
		'authorsite'	=> 'http://ab-plugins.cc',
		'version'	=> '1.0',
		'compatibility'	=> '18*',
		'codename'	=> CN_ABPNOTICE
	);
}

function notice_activate ()
{
	global $db;
	$new_stylesheet = array(
        'name'         => CN_ABPNOTICE.'.css',
        'tid'          => 1,
        'attachedto'   => '',
        'stylesheet'   => '.noticeui {
	color: #FFF;width: 83%;font-weight: normal;padding: 0 0 15px 15px;margin-bottom: 2.5em;
	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;
	border-radius: 6px;
	-moz-box-shadow: 1px 1px 2px rgba(0,0,0,.4);
	-webkit-box-shadow: 1px 1px 2px rgba(0,0,0,.4);
	box-shadow: 1px 1px 2px rgba(0,0,0,.4);
	position: relative;
	left: 34px;
}
.noticeui p { margin: 0 0 1.5em 0;}
.noticeui p:last-child  { margin-bottom: 0; }
.noticeui ul { margin-left: 8px; margin-bottom: 1.5em; }
.noticeui ul:last-child { margin-bottom: 0; }
.noticeui li { padding-left: 18px; margin-bottom: .75em; }
.noticeui h5 { font-size: 14px; font-weight: bold; margin: 0 0 .65em 0; }
.noticeui-success { background-color: #EEF4D4; color: #596C26; border: 1px solid #8FAD3D; }
.noticeui-warn { background-color: #FFEA97; color: #796100; border: 1px solid #E1B500; }
.noticeui-error { background-color: #EFCEC9; color: #933628; border: 1px solid #AE3F2F; }
.noticeui-info { background-color: #C6D8F0; color: #285797; border: 1px solid #4381CD; }
',
        'lastmodified' => TIME_NOW
    );

    $sid = $db->insert_query('themestylesheets', $new_stylesheet);
    $db->update_query('themestylesheets', array('cachefile' => "css.php?stylesheet={$sid}"), "sid='{$sid}'", 1);

    $query = $db->simple_select('themes', 'tid');
    while($theme = $db->fetch_array($query))
    {
        require_once MYBB_ADMIN_DIR.'inc/functions_themes.php';
        update_theme_stylesheet_list($theme['tid']);
    }
}
    
    
function notice_deactivate ()
{
	global $db;
    $db->delete_query('themestylesheets', "name='".CN_ABPNOTICE.".css'");
    $query = $db->simple_select('themes', 'tid');
    while($theme = $db->fetch_array($query))
    {
        require_once MYBB_ADMIN_DIR.'inc/functions_themes.php';
        update_theme_stylesheet_list($theme['tid']);
    } 
}

function notice_run ($message)
{
	$message = preg_replace(
		'!\[(warn|success|error|info)\](.*)\[/\1\]!Usi',
		'<div class="noticeui noticeui-$1"><p>$2</p></div>',
		$message
	);
	$message = preg_replace(
		'!\[(warn|success|error|info)=(.+)\](.*)\[/\1\]!Usi',
		'<div class="noticeui noticeui-$1"><h5>$2</h5><p>$3</p></div>',
		$message
	);
	return $message;
}

?>
